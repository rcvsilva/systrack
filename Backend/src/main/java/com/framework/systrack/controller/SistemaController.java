package com.framework.systrack.controller;

import com.framework.systrack.domain.Sistema;
import com.framework.systrack.repository.SistemaRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Log
@Service
@RestController
@RequestMapping("/api")
public class SistemaController {

    @Autowired
    SistemaRepository sistemaRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getIndexPage() {
        return new ModelAndView("/index");
    }

    @GetMapping(value = "/sistemas")
    public List<Sistema> findAll() {
        return sistemaRepository.findAll();
    }

    @GetMapping(value = "/sistema/{id}")
    public Sistema findOne(@PathVariable long id) {
        return sistemaRepository.findById(id);
    }

    @PostMapping("/sistema")
    public Sistema addSistema(@RequestBody Sistema sistema) {
        return sistemaRepository.save(sistema);
    }

    @DeleteMapping("/sistema")
    public void deleteSistema(@RequestBody Sistema sistema) {
        sistemaRepository.delete(sistema);
    }

    @DeleteMapping("/sistema/{id}")
    public void deleteById(@PathVariable(value = "id") Long id) {
        sistemaRepository.deleteById(id);
    }

    @PutMapping("/sistema")
    public Sistema updateSistema(@RequestBody Sistema sistema) {
        return sistemaRepository.save(sistema);
    }
}