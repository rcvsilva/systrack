package com.framework.systrack.repository;

import com.framework.systrack.domain.Erro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ErroRepository extends JpaRepository<Erro, Long> {
    Erro findById(long id);
}
