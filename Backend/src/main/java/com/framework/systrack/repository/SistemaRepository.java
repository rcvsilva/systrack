package com.framework.systrack.repository;

import com.framework.systrack.domain.Sistema;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SistemaRepository extends JpaRepository<Sistema, Long> {

    Sistema findById(long id);
}