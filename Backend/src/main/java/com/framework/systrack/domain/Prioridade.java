package com.framework.systrack.domain;


import lombok.Getter;

@Getter
public enum Prioridade {
    ALTA,
    MEDIA,
    BAIXA
}