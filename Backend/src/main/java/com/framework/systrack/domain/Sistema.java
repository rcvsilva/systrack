package com.framework.systrack.domain;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;

import javax.persistence.*;

import com.sun.istack.internal.NotNull;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sistema")
@EntityListeners(AuditingEntityListener.class)

public class Sistema implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_sistema;

    private String nome_sistema;

    private String descricao_sistema;

    private String endereco_sistema;

    private Integer status_sistema;

    @NotNull
    private Prioridade type;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "sistema_responsavel",
            joinColumns = @JoinColumn(name = "id_sistema"),
            inverseJoinColumns = @JoinColumn(name = "id_resp")
    )
    private List<Responsavel> responsaveis = new ArrayList<>();


    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "sistema_erro",
            joinColumns = @JoinColumn(name = "id_sistema"),
            inverseJoinColumns = @JoinColumn(name = "id_erro")
    )
    private List<Erro> erro = new ArrayList<>();

}