package com.framework.systrack.domain;

import javax.persistence.*;

import com.sun.istack.internal.NotNull;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "erro")
@EntityListeners(AuditingEntityListener.class)

public class Erro {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_erro;

    private String descricao_erro;
}